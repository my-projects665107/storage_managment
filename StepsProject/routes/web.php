<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::view('/','login')->name('login');
Route::post('/login',[AuthController::class,'login'])->name('userLogin');
Route::get('/logout',[AuthController::class,'logout'])->name('logout');
Route::view('/mainDashboard','main.dashboard')->name('dashboard');
Route::resource('users', UserController::class)->middleware('can:isAdmin');
Route::resource('products', ProductController::class);
