<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function managedUser(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'product_tracking'
            ,'product_id'
            ,'user_id'

        );
    }

    public function movedTo(): BelongsToMany
    {
        return $this->belongsToMany(
            Stroage::class,
            'product_tracking',
            'product_id',
            'stoarge_id',
        )->withPivot(['quantity']);
    }
}
