<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class StorageCategory extends Model
{
    use HasFactory;

    public function storages(): HasMany
    {
        return $this->hasMany(Storage::class);
    }
}
