<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Stroage extends Model
{
    use HasFactory;

    public function place(): BelongsTo
    {
        return $this->belongsTo(Place::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(StorageCategory::class);
    }

    public function movedIn(): BelongsToMany
    {
        return $this->belongsToMany(
          Product::class,
          'product_tracking',
          'stoarge_id',
          'product_id',
        )->withPivot(['product_id']);
    }

}
