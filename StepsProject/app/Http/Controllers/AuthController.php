<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validation =  $request->validate([
           'email' => 'required|email',
           'password' => 'required|string'
        ]);

        if (Auth::attempt($validation)){
            session()->regenerate();
            session()->regenerateToken();
            return view('main.dashboard');
        }else return back()->withErrors('Invalid Credentials');
    }
    public function logout()
    {
        session()->invalidate();
        session()->regenerateToken();
        return redirect("/");
    }
}
