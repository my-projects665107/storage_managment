<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users =  User::where('id','!=',Auth::id())->paginate(10);
        return view('management.users',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = null;
        return view('components.user-form', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->role_id = $request->role;
        $user->save();
        return to_route('users.index');

    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('components.user-form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $user->name = $request->name ?? $user->name;
        $user->email = $request->email ?? $user->email;
        $user->role_id = $request->role ?? $user->role_id;
        $user->password = Hash::make($request->password) ?? Hash::make($user->password);
        $user->save();
        return to_route('users.index');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $this->anonymous($user);
        $user->delete();
        return to_route('users.index');
    }

    private function anonymous(User $user)
    {
        $text = 'XXXXXXXXXXX';
        $user->name = $text;
        $user->email = Hash::make($text);
        $user->password = $text;
        $user->save();
    }
}
