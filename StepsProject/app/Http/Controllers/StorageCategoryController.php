<?php

namespace App\Http\Controllers;

use App\Models\StorageCategory;
use Illuminate\Http\Request;

class StorageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(StorageCategory $storageCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(StorageCategory $storageCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, StorageCategory $storageCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StorageCategory $storageCategory)
    {
        //
    }
}
