@extends('layout.sub')
@section('body')

    <div class="m-3">
        @if( $user == null )
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old('email')}}">
                </div>

                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="name" id="name" value="{{old('name')}}">
                </div>

                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1">
                </div>

                <select name="role" class="form-select" aria-label="Default select example">
                    <option>Open this select menu</option>
                    <option value="1">Admin</option>
                    <option value="2">Management</option>
                    <option value="3">Employee</option>
                </select>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        @else
            <form action="{{route('users.update',$user->id)}}" method="post">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$user->email}}">
                </div>

                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="name" id="name" value="{{$user->name}}">
                </div>

                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1">
                </div>

                <select name="role" class="form-select" aria-label="Default select example">
                    <option>Open this select menu</option>
                    <option value="1">Admin</option>
                    <option value="2">Management</option>
                    <option value="3">Employee</option>
                </select>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        @endif
    </div>





@endsection

