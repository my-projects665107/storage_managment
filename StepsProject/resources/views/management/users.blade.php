@extends('layout.main')
@section('body')

    <div class="mx-3">
        <a href="{{route('users.create')}}">
            <i class="bi bi-plus-circle-dotted fs-1 "></i>
        </a>
    </div>

    <hr>



    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Options</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role->type}}</td>
                <td>
                    <div class="dropdown">
                        <i class="bi bi-pencil" type="button" data-bs-toggle="dropdown" aria-expanded="false"></i>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{route('users.edit',$user->id)}}">Edit</a></li>
                            <li>
                                <form action="{{route('users.destroy',$user->id)}}" class="dropdown-item"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit">Delete</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>






    <div class="d-flex justify-content-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="{{$users->previousPageUrl()}}">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="{{$users->nextPageUrl()}}">Next</a></li>
            </ul>
        </nav>
    </div>

@endsection
