@extends('layout.main')
@section('body')

    <div class="mx-3">
        <a href="{{route('users.create')}}">
            <i class="bi bi-plus-circle-dotted fs-1 "></i>
        </a>
    </div>

    <hr>
    <table>
        <tr>
            <th scope="col">Lager Name</th>
            <th scope="col">Product</th>
            <th scope="col">Anzahl</th>
        </tr>
        @foreach ($storages as $storage)
{{--            @dd($storage->movedIn)--}}
            <tr>
                <th>{{$storage->name}}</th>
{{--                <th>{{$storage->movedIn->where($products->id == 'pivot.product_id')}}</th>--}}
                <th>{{$storage->movedIn->sum('pivot.product_id')}}</th>
            </tr>
        @endforeach
    </table>

    <table>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Amount Moved</th>
        </tr>
        <tr>
            <th>{{$users->name}}</th>
            <th>{{$users->email}}</th>
            <th>{{$users->managed_product_count}}</th>
        </tr>
    </table>
    <table>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Amount Moved</th>
            <th scope="col">Lager</th>
        </tr>
        @foreach($extrausers as $extrauser)

        <tr>
            <th>{{$extrauser}}</th>
            <th>{{$extrauser->amount}}</th>
            <th>{{$extrauser->lager}}</th>
        </tr>
            @endforeach
    </table>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Bild</th>
            <th scope="col">Beschreibung</th>
            <th scope="col">Category</th>
            <th scope="col">Movement Amount</th>
            <th scope="col">Amount Moved</th>
            <th scope="col">User Moved</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)

            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->picture}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->category->name}}</td>
                <td>{{$product->movedTo->count()}}</td>
                <td>{{$product->movedTo->sum('pivot.quantity')}}</td>
                <td>{{$product->managedUser->count('user_id')}}</td>
                {{--                <td>--}}
                {{--                    <div class="dropdown">--}}
                {{--                        <i class="bi bi-pencil" type="button" data-bs-toggle="dropdown" aria-expanded="false"></i>--}}
                {{--                        <ul class="dropdown-menu">--}}
                {{--                            <li><a class="dropdown-item" href="{{route('$products.edit',$product->id)}}">Edit</a></li>--}}
                {{--                            <li>--}}
                {{--                                <form action="{{route('products.destroy',$product->id)}}" class="dropdown-item"--}}
                {{--                                      method="post">--}}
                {{--                                    @csrf--}}
                {{--                                    @method('DELETE')--}}
                {{--                                    <button type="submit">Delete</button>--}}
                {{--                                </form>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </td>--}}
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection




{{--    <div class="d-flex justify-content-center">--}}
{{--        <nav aria-label="Page navigation example">--}}
{{--            <ul class="pagination">--}}
{{--                <li class="page-item"><a class="page-link" href="{{$product->previousPageUrl()}}">Previous</a>--}}
{{--                </li>--}}
{{--                <li class="page-item"><a class="page-link" href="{{$product->nextPageUrl()}}">Next</a></li>--}}
{{--            </ul>--}}
{{--        </nav>--}}
{{--    </div>--}}

