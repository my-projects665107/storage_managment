<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $amount = 21;
        $pro = ( $amount * 5 ) / 100;

        $admin = new User();
        $admin->name = "admin";
        $admin->email = "admin@mail.com";
        $admin->password = Hash::make('123');
        $admin->role_id = 1;
        $admin->save();

        for ($i = 2; $i < $amount; $i++) {
            $user = new User();
            $user->name = $faker->unique()->name();
            $user->email = $faker->unique()->email();
            $user->password = Hash::make($faker->unique()->password());
            $user->role_id = $faker->numberBetween(1,3);
            $user->save();
        }
    }
}
