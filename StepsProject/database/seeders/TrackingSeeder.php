<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrackingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();


        for ($i = 0; $i < 1000; $i++) {
            $randomUserId = $faker->numberBetween(1,20);
            $randomStorageId = $faker->numberBetween(1,10);
            $randomProductId = $faker->numberBetween(1,101);
            $user = User::find($randomUserId);



            $randomTimeLastYear = Carbon::now()->subYear()->addSeconds(rand(0, Carbon::now()->subYear()->diffInSeconds()));
            $randomTimeLastMonth = Carbon::now()->subMonth()->addSeconds(rand(0, Carbon::now()->subMonth()->diffInSeconds()));
            $randomTimeLastWeek = Carbon::now()->subWeek()->addSeconds(rand(0, Carbon::now()->subWeek()->diffInSeconds()));

            $time =  now()->subSeconds(rand(0, 365 * 24 * 60 * 60));
            if ($i % 5 == 0) $time = $randomTimeLastYear;
            if ($i % 9 == 0) $time = $randomTimeLastMonth;
            if ($i % 12 == 0) $time = $randomTimeLastWeek;



            $user->managedProduct()->attach($randomProductId,
                [
                    'quantity'   => $faker->numberBetween(1,20),
                    'stoarge_id' => $randomStorageId,
                    'created_at' => $time,
                ]
            );



        }
    }
}
