<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = new Role();
        $management = new Role();
        $employee = new Role();

        $admin->type = "ADMIN";
        $management->type = "MANAGEMENT";
        $employee->type = 'EMPLOYEE';

        $admin->save();
        $management->save();
        $employee->save();
    }
}
