<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use App\Models\StorageCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 1; $i < 22; $i++) {
            $productCategory = new ProductCategory();
            $productCategory->name = 'Product_Category_'.$i;
            $productCategory->created_at = now();
            $productCategory->save();
        }
    }
}
