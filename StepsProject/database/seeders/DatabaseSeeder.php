<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
      $roles = new RolesSeeder();
      $users = new UsersSeeder();
      $products_category = new ProductsCategorySeeder();
      $products = new ProductsSeeder();
      $places = new PlacesSeeder();
      $storage_category = new StorageCategoriesSeeder();
      $storages = new StorageSeeder();
      $tracking = new TrackingSeeder();

      $roles->run();
      $users->run();
      $products_category->run();
      $products->run();
      $places->run();
      $storage_category->run();
      $storages->run();
      $tracking->run();
    }
}
