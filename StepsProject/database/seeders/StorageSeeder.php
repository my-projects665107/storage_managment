<?php

namespace Database\Seeders;

use App\Models\Stroage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Storage;

class StorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 1; $i < 11; $i++) {
            $storage = new Stroage();
            $storage->name = $faker->unique()->name();
            $storage->category_id = $faker->numberBetween(1,20);
            $storage->place_id = $faker->numberBetween(1,30);
            $storage->created_at = now();
            $storage->save();
        }
    }
}
