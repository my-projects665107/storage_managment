<?php

namespace Database\Seeders;

use App\Models\StorageCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StorageCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 1; $i < 22; $i++) {
            $storageCategory = new StorageCategory();
            $storageCategory->name = 'Storage_Category_'.$i;
            $storageCategory->created_at = now();
            $storageCategory->save();
        }

    }
}
