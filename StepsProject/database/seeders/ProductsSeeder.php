<?php

namespace Database\Seeders;

use App\Models\Product;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 1; $i < 102; $i++) {
            $product = new Product();
            $product->name = $faker->unique()->word();
            $product->picture = $faker->imageUrl();
            $product->description = $faker->text(50);
            $product->category_id = $faker->numberBetween(1,20);
            $product->created_at = now();
            $product->save();
        }
    }
}
